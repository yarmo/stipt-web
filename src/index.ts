import { version } from "../package.json";
import { navBar } from "./components/navbar";
import { Context } from "./context";
import { changeLanguage, t } from "./i18n";
import "./main.scss";
import { appPage } from "./pages/app";
import { landingPage } from "./pages/landing";
import { changePage } from "./router";

const StiptApp = async () => {
	const ctx = Context.new();
	await ctx.load();

	const { van } = ctx;
	const { h1, p, a, br, div, span, main, header, footer } = van.tags;

	console.log("initial load", ctx);

	van.derive(() => changePage(ctx, ctx.activePage.val));
	van.derive(() => changeLanguage(ctx.language.val));

	const page = ctx.loggedInUser.val ? appPage(ctx) : landingPage(ctx);

	return main(
		header(
			h1(() => {
				if (!ctx.displayTime.val) return span();
				return span(
					{ class: "timestamp" },
					ctx.displayTime.val.toLocaleTimeString("nl-NL", {
						hour: "2-digit",
						minute: "2-digit",
					}),
				);
			}, "Stipt"),
			() => navBar(ctx),
		),
		div({ id: "layout" }, page),
		footer(() =>
			p(
				t("version"),
				" ",
				version,
				br,
				"Build: ",
				// @ts-ignore
				BUILD_TIMESTAMP,
				br,
				t("language"),
				": ",
				ctx.language.val,
				br,
				a({ href: "https://codeberg.org/yarmo/setipa-web" }, "source code"),
			),
		),
	);
};

const main = async () => {
	document.body.replaceChildren(await StiptApp());
};

main();
