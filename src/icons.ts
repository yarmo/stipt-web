import van from "vanjs-core";

const { svg, path, line, rect, circle, polyline, polygon } = van.tagsNS(
	"http://www.w3.org/2000/svg",
);

export const play = () =>
	svg(
		{
			xmlns: "http://www.w3.org/2000/svg",
			width: "24",
			height: "24",
			viewBox: "0 0 24 24",
			fill: "none",
			stroke: "currentColor",
			"stroke-width": "2",
			"stroke-linecap": "round",
			"stroke-linejoin": "round",
			class: "feather feather-play",
		},
		polygon({ points: "5 3 19 12 5 21 5 3" }),
	);
export const stop = () =>
	svg(
		{
			xmlns: "http://www.w3.org/2000/svg",
			width: "24",
			height: "24",
			viewBox: "0 0 24 24",
			fill: "none",
			stroke: "currentColor",
			"stroke-width": "2",
			"stroke-linecap": "round",
			"stroke-linejoin": "round",
			class: "feather feather-square",
		},
		rect({ x: "3", y: "3", width: "18", height: "18", rx: "2", ry: "2" }),
	);
export const check = () =>
	svg(
		{
			xmlns: "http://www.w3.org/2000/svg",
			width: "24",
			height: "24",
			viewBox: "0 0 24 24",
			fill: "none",
			stroke: "currentColor",
			"stroke-width": "2",
			"stroke-linecap": "round",
			"stroke-linejoin": "round",
			class: "feather feather-check",
		},
		polyline({ points: "20 6 9 17 4 12" }),
	);
export const info = () =>
	svg(
		{
			xmlns: "http://www.w3.org/2000/svg",
			width: "24",
			height: "24",
			viewBox: "0 0 24 24",
			fill: "none",
			stroke: "currentColor",
			"stroke-width": "2",
			"stroke-linecap": "round",
			"stroke-linejoin": "round",
			class: "feather feather-info",
		},
		circle({ cx: "12", cy: "12", r: "10" }),
		line({ x1: "12", y1: "16", x2: "12", y2: "12" }),
		line({ x1: "12", y1: "8", x2: "12.01", y2: "8" }),
	);
export const chevronUp = () =>
	svg(
		{
			xmlns: "http://www.w3.org/2000/svg",
			width: "24",
			height: "24",
			viewBox: "0 0 24 24",
			fill: "none",
			stroke: "currentColor",
			"stroke-width": "2",
			"stroke-linecap": "round",
			"stroke-linejoin": "round",
			class: "feather feather-chevron-up",
		},
		polyline({ points: "18 15 12 9 6 15" }),
	);
export const chevronDown = () =>
	svg(
		{
			xmlns: "http://www.w3.org/2000/svg",
			width: "24",
			height: "24",
			viewBox: "0 0 24 24",
			fill: "none",
			stroke: "currentColor",
			"stroke-width": "2",
			"stroke-linecap": "round",
			"stroke-linejoin": "round",
			class: "feather feather-chevron-down",
		},
		polyline({ points: "6 9 12 15 18 9" }),
	);
export const arrowUp = () =>
	svg(
		{
			xmlns: "http://www.w3.org/2000/svg",
			width: "24",
			height: "24",
			viewBox: "0 0 24 24",
			fill: "none",
			stroke: "currentColor",
			"stroke-width": "2",
			"stroke-linecap": "round",
			"stroke-linejoin": "round",
			class: "feather feather-arrow-up",
		},
		line({ x1: "12", y1: "19", x2: "12", y2: "5" }),
		polyline({ points: "5 12 12 5 19 12" }),
	);
export const arrowDown = () =>
	svg(
		{
			xmlns: "http://www.w3.org/2000/svg",
			width: "24",
			height: "24",
			viewBox: "0 0 24 24",
			fill: "none",
			stroke: "currentColor",
			"stroke-width": "2",
			"stroke-linecap": "round",
			"stroke-linejoin": "round",
			class: "feather feather-arrow-down",
		},
		line({ x1: "12", y1: "5", x2: "12", y2: "19" }),
		polyline({ points: "19 12 12 19 5 12" }),
	);
export const plusCircle = () =>
	svg(
		{
			xmlns: "http://www.w3.org/2000/svg",
			width: "24",
			height: "24",
			viewBox: "0 0 24 24",
			fill: "none",
			stroke: "currentColor",
			"stroke-width": "2",
			"stroke-linecap": "round",
			"stroke-linejoin": "round",
			class: "feather feather-plus-circle",
		},
		circle({ cx: "12", cy: "12", r: "10" }),
		line({ x1: "12", y1: "8", x2: "12", y2: "16" }),
		line({ x1: "8", y1: "12", x2: "16", y2: "12" }),
	);
export const x = () =>
	svg(
		{
			xmlns: "http://www.w3.org/2000/svg",
			width: "24",
			height: "24",
			viewBox: "0 0 24 24",
			fill: "none",
			stroke: "currentColor",
			"stroke-width": "2",
			"stroke-linecap": "round",
			"stroke-linejoin": "round",
			class: "feather feather-x",
		},
		line({ x1: "18", y1: "6", x2: "6", y2: "18" }),
		line({ x1: "6", y1: "6", x2: "18", y2: "18" }),
	);
export const list = () =>
	svg(
		{
			xmlns: "http://www.w3.org/2000/svg",
			width: "24",
			height: "24",
			viewBox: "0 0 24 24",
			fill: "none",
			stroke: "currentColor",
			"stroke-width": "2",
			"stroke-linecap": "round",
			"stroke-linejoin": "round",
			class: "feather feather-list",
		},
		line({ x1: "8", y1: "6", x2: "21", y2: "6" }),
		line({ x1: "8", y1: "12", x2: "21", y2: "12" }),
		line({ x1: "8", y1: "18", x2: "21", y2: "18" }),
		line({ x1: "3", y1: "6", x2: "3.01", y2: "6" }),
		line({ x1: "3", y1: "12", x2: "3.01", y2: "12" }),
		line({ x1: "3", y1: "18", x2: "3.01", y2: "18" }),
	);
export const user = () =>
	svg(
		{
			xmlns: "http://www.w3.org/2000/svg",
			width: "24",
			height: "24",
			viewBox: "0 0 24 24",
			fill: "none",
			stroke: "currentColor",
			"stroke-width": "2",
			"stroke-linecap": "round",
			"stroke-linejoin": "round",
			class: "feather feather-user",
		},
		path({ d: "M20 21v-2a4 4 0 0 0-4-4H8a4 4 0 0 0-4 4v2" }),
		circle({ cx: "12", cy: "7", r: "4" }),
	);
export const edit = () =>
	svg(
		{
			xmlns: "http://www.w3.org/2000/svg",
			width: "24",
			height: "24",
			viewBox: "0 0 24 24",
			fill: "none",
			stroke: "currentColor",
			"stroke-width": "2",
			"stroke-linecap": "round",
			"stroke-linejoin": "round",
			class: "feather feather-edit",
		},
		path({ d: "M11 4H4a2 2 0 0 0-2 2v14a2 2 0 0 0 2 2h14a2 2 0 0 0 2-2v-7" }),
		path({ d: "M18.5 2.5a2.121 2.121 0 0 1 3 3L12 15l-4 1 1-4 9.5-9.5z" }),
	);
export const mapPin = () =>
	svg(
		{
			xmlns: "http://www.w3.org/2000/svg",
			width: "24",
			height: "24",
			viewBox: "0 0 24 24",
			fill: "none",
			stroke: "currentColor",
			"stroke-width": "2",
			"stroke-linecap": "round",
			"stroke-linejoin": "round",
			class: "feather feather-map-pin",
		},
		path({ d: "M21 10c0 7-9 13-9 13s-9-6-9-13a9 9 0 0 1 18 0z" }),
		circle({ cx: "12", cy: "10", r: "3" }),
	);
export const phone = () =>
	svg(
		{
			xmlns: "http://www.w3.org/2000/svg",
			width: "24",
			height: "24",
			viewBox: "0 0 24 24",
			fill: "none",
			stroke: "currentColor",
			"stroke-width": "2",
			"stroke-linecap": "round",
			"stroke-linejoin": "round",
			class: "feather feather-phone",
		},
		path({
			d: "M22 16.92v3a2 2 0 0 1-2.18 2 19.79 19.79 0 0 1-8.63-3.07 19.5 19.5 0 0 1-6-6 19.79 19.79 0 0 1-3.07-8.67A2 2 0 0 1 4.11 2h3a2 2 0 0 1 2 1.72 12.84 12.84 0 0 0 .7 2.81 2 2 0 0 1-.45 2.11L8.09 9.91a16 16 0 0 0 6 6l1.27-1.27a2 2 0 0 1 2.11-.45 12.84 12.84 0 0 0 2.81.7A2 2 0 0 1 22 16.92z",
		}),
	);
export const trash = () =>
	svg(
		{
			xmlns: "http://www.w3.org/2000/svg",
			width: "24",
			height: "24",
			viewBox: "0 0 24 24",
			fill: "none",
			stroke: "currentColor",
			"stroke-width": "2",
			"stroke-linecap": "round",
			"stroke-linejoin": "round",
			class: "feather feather-trash",
		},
		polyline({ points: "3 6 5 6 21 6" }),
		path({
			d: "M19 6v14a2 2 0 0 1-2 2H7a2 2 0 0 1-2-2V6m3 0V4a2 2 0 0 1 2-2h4a2 2 0 0 1 2 2v2",
		}),
	);
export const slash = () =>
	svg(
		{
			xmlns: "http://www.w3.org/2000/svg",
			width: "24",
			height: "24",
			viewBox: "0 0 24 24",
			fill: "none",
			stroke: "currentColor",
			"stroke-width": "2",
			"stroke-linecap": "round",
			"stroke-linejoin": "round",
			class: "feather feather-slash",
		},
		circle({ cx: "12", cy: "12", r: "10" }),
		line({ x1: "4.93", y1: "4.93", x2: "19.07", y2: "19.07" }),
	);
