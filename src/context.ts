import { Models } from "appwrite";
import van, { State, Van } from "vanjs-core";
import { AppointmentState } from "./components/appointment";
import { computeProgress, getCurrentWeekday } from "./helpers";
import {
	addAppointment,
	deleteAppointment,
	getAppointments,
	getCurrentAccount,
	getPrefs,
	setPrefs,
	updateAppointment,
	updateAppointmentOrder,
} from "./server";

let intervalId: number | null = null;

export class Context {
	constructor(
		public van: Van,
		public appointments: State<AppointmentState[]>[],
		public language: State<string>,
		public displayTime: State<Date | null>,
		public lastStartTime: State<Date | null>,
		public computedEndTime: State<Date | null>,
		public activeDay: State<number>,
		public activePage: State<string>,
		public loggedInUser: State<Models.User<Models.Preferences> | null>,
	) {
		this.van = van;
		this.appointments = appointments;
		if (this.appointments.length !== 7)
			this.appointments = [
				van.state([]),
				van.state([]),
				van.state([]),
				van.state([]),
				van.state([]),
				van.state([]),
				van.state([]),
			];
		this.displayTime = displayTime;
		this.lastStartTime = lastStartTime;
		this.computedEndTime = computedEndTime;
		this.activeDay = activeDay;
		this.activePage = activePage;
		this.loggedInUser = loggedInUser;
	}

	static new = () => {
		const ctx = new Context(
			van,
			[],
			van.state("en-US"),
			van.state(null),
			van.state(null),
			van.state(null),
			van.state(getCurrentWeekday()),
			van.state("landing"),
			van.state(null),
		);
		return ctx;
	};

	load = async () => {
		console.log("loading");

		const acc = await getCurrentAccount();
		if (!acc) return;

		const prefs = (await getPrefs()) ?? {};
		const appointmentsDb = await getAppointments();
		const appointments: State<AppointmentState[]>[] = [
			this.van.state([]),
			this.van.state([]),
			this.van.state([]),
			this.van.state([]),
			this.van.state([]),
			this.van.state([]),
			this.van.state([]),
		];

		try {
			prefs.lastStartTime = prefs.lastStartTime
				? new Date(prefs.lastStartTime)
				: null;

			appointmentsDb?.documents.forEach((a) => {
				appointments[a.weekday].val.push(
					new AppointmentState(
						a.$id,
						this.van.state(a.order),
						this.van.state(0),
						this.van.state(a.title),
						this.van.state(a.notes),
						this.van.state(a.address),
						this.van.state(a.phone),
						this.van.state(a.category ?? 0),
						this.van.state(a.duration),
						this.van.state(a.durationOffset),
						this.van.state(0),
						this.van.state(false),
						this.van.state(""),
						this.van.state(-1),
						this.van.state(false),
					),
				);
			});
			appointments.forEach((d) => {
				d.val.forEach((a) => {
					a.totalN.val = d.val.length;
				});
			});
		} catch (err) {
			console.warn("Could not load context", err);
			return;
		}

		const displayTime = prefs.lastStartTime ? new Date() : null;
		const language = prefs.language ?? "en-US";

		this.appointments = appointments;
		this.language.val = language;
		this.displayTime.val = displayTime;
		this.lastStartTime.val = prefs.lastStartTime;
		this.activeDay.val = getCurrentWeekday();
		this.activePage.val = "app";
		this.loggedInUser.val = acc;

		this.updateIntervalTimer();
	};

	onUpdate() {
		console.log("onUpdate", this);

		this.appointments = this.appointments
			// Move appointment up
			.map((d) => {
				const indexMoveUp = d.val.findIndex((e) => e.move.val === -1);
				if (indexMoveUp > -1 && indexMoveUp !== 0) {
					const el = d.val.splice(indexMoveUp, 1)[0];
					d.val.splice(indexMoveUp - 1, 0, el);
					d.val[indexMoveUp - 1].move.val = 0;
				}
				return d;
			})
			// Move appointment down
			.map((d) => {
				const indexMoveDown = d.val.findIndex((e) => e.move.val === 1);
				if (indexMoveDown > -1 && indexMoveDown !== d.val.length - 1) {
					const el = d.val.splice(indexMoveDown, 1)[0];
					d.val.splice(indexMoveDown + 1, 0, el);
					d.val[indexMoveDown + 1].move.val = 0;
				}
				return d;
			})
			// Delete appointments
			.map((d) => {
				const copy = d.val.filter((o) => {
					const f = o.deleted.val;
					if (f) {
						deleteAppointment(o.id);
					}
					return !f;
				});
				if (d.val.length !== copy.length) {
					d.val = copy;
				}
				return d;
			})
			// Reapply data
			.map((d) => {
				let changeDetected = false;
				const copy = d.val.map((a, i) => {
					// order
					if (a.order.val !== i) {
						a.order.val = i;
						changeDetected = true;
						updateAppointmentOrder(a.id, i);
					}
					// totalN
					if (a.totalN.val !== d.val.length) {
						a.totalN.val = d.val.length;
						changeDetected = true;
					}
					return a;
				});

				if (changeDetected) {
					d.val = copy;
				}
				return d;
			});

		// Update start times
		this.computeStartTimesAndProgress();
	}

	onInterval() {
		console.log("onInterval", this);
		if (this.displayTime.val) this.displayTime.val = new Date();
		this.computeStartTimesAndProgress();
	}

	async addAppointment(
		title: string,
		notes: string,
		address: string,
		phone: string,
		category: number,
		duration: number,
		dayIndex: number,
	) {
		const a = await addAppointment(
			title,
			notes,
			address,
			phone,
			category,
			duration,
			dayIndex,
			this.appointments[dayIndex].val.length,
		);
		if (!a) return;
		const totalN = this.appointments[this.activeDay.val].val.length;
		this.appointments[dayIndex].val.push(
			new AppointmentState(
				a.$id,
				van.state(a.order),
				van.state(totalN),
				van.state(a.title),
				van.state(a.notes),
				van.state(a.address),
				van.state(a.phone),
				van.state(a.category ?? 0),
				van.state(a.duration),
				van.state(a.durationOffset),
				van.state(0),
				van.state(false),
				van.state(""),
				van.state(-1),
				van.state(false),
			),
		);
	}

	async editAppointment(
		id: string,
		title: string,
		notes: string,
		address: string,
		phone: string,
		category: number,
		duration: number,
		dayIndex: number,
	) {
		const index = this.appointments[dayIndex].val.findIndex((x) => x.id === id);

		this.appointments[dayIndex].val[index].title.val = title;
		this.appointments[dayIndex].val[index].notes.val = notes;
		this.appointments[dayIndex].val[index].address.val = address;
		this.appointments[dayIndex].val[index].phone.val = phone;
		this.appointments[dayIndex].val[index].category.val = category;
		this.appointments[dayIndex].val[index].duration.val = duration;

		await updateAppointment(id, this.appointments[dayIndex].val[index]);
	}

	start() {
		console.log("start");

		this.lastStartTime.val = new Date();
		this.displayTime.val = new Date();
		this.computeStartTimesAndProgress();
		this.onUpdate();
		this.savePrefs();

		this.updateIntervalTimer();
	}

	stop() {
		console.log("stop");

		this.lastStartTime.val = null;
		this.computedEndTime.val = null;
		this.displayTime.val = null;
		this.appointments = this.appointments.map((d) => {
			d.val.forEach((o) => {
				o.progress.val = -1;
				o.startTime.val = "";
				return o;
			});
			return d;
		});
		this.onUpdate();
		this.savePrefs();

		this.updateIntervalTimer();
	}

	updateIntervalTimer() {
		if (this.lastStartTime.val) {
			if (!intervalId) {
				intervalId = setInterval(() => {
					this.onInterval();
				}, 3000);
			}
		} else {
			if (intervalId) {
				clearInterval(intervalId);
				intervalId = null;
			}
		}
	}

	reset() {
		console.log("reset");

		const dayIndex = (new Date().getDay() + 6) % 7;
		this.appointments[dayIndex].val = this.appointments[dayIndex].val.map(
			(o) => {
				o.durationOffset.val = 0;
				return o;
			},
		);
		this.onUpdate();
	}

	computeStartTimesAndProgress() {
		console.log("compute start times");

		if (!this.lastStartTime.val) return

		const now = new Date();
		const dayIndex = (now.getDay() + 6) % 7;
		let nextTimestamp = this.lastStartTime.val;
		this.appointments[dayIndex].val = this.appointments[dayIndex].val.map(
			(o) => {
				o.startTime.val = nextTimestamp.toLocaleTimeString("nl-NL", {
					hour: "2-digit",
					minute: "2-digit",
				});
				const endTime = new Date(
					nextTimestamp.getTime() +
						(o.duration.val + o.durationOffset.val) * 60000,
				);
				o.progress.val = computeProgress(nextTimestamp, endTime, now);
				nextTimestamp = endTime;
				return o;
			},
		);

		if (
			!this.computedEndTime.val ||
			this.computedEndTime.val !== nextTimestamp
		) {
			this.computedEndTime.val = nextTimestamp;
		}
	}

	simulateStartTimes(time: String) {
		console.log("simulate start times");

		if (!time) return
		if (this.lastStartTime.val) return

		const now = new Date();
		const dayIndex = (now.getDay() + 6) % 7;
		let nextTimestamp = now;
		nextTimestamp.setHours(parseInt(time.split(":")[0]));
		nextTimestamp.setMinutes(parseInt(time.split(":")[1]));
		nextTimestamp.setSeconds(0);
		this.appointments[dayIndex].val = this.appointments[dayIndex].val.map(
			(o) => {
				o.startTime.val = nextTimestamp.toLocaleTimeString("nl-NL", {
					hour: "2-digit",
					minute: "2-digit",
				});
				const endTime = new Date(
					nextTimestamp.getTime() +
						(o.duration.val + o.durationOffset.val) * 60000,
				);
				o.progress.val = 0;
				nextTimestamp = endTime;
				return o;
			},
		);

		if (
			!this.computedEndTime.val ||
			this.computedEndTime.val !== nextTimestamp
		) {
			this.computedEndTime.val = nextTimestamp;
		}
	}

	savePrefs() {
		setPrefs(this);
	}
}
