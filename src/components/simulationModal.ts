import { Context } from "../context";
import { t } from "../i18n";
import * as icons from "../icons";

export const showSimulationModal = () => {
	const el: HTMLDialogElement | null =
		document.querySelector("#simulationModal");
	if (!el) return;

	el.showModal();
};

export const simulationModal = (ctx: Context) => {
	const { div, h3, span, button, dialog, form, input, textarea, label } =
		ctx.van.tags;

	const buttonCloseDom = button(
		{
			class: "close",
			onclick: () => {
				(
					document.querySelector("#simulationModal") as HTMLDialogElement
				).close();
			},
		},
		icons.x,
	);
	const inputTimeDom = input({ class: "time", type: "time" });

	return dialog(
		{ id: "simulationModal" },
		buttonCloseDom,
		form(
			{
				method: "dialog",
				onsubmit: async (evt: Event) => {
					evt.preventDefault();

					// const selectedCategory = parseInt(
					// 	// @ts-ignore
					// 	document.querySelector("input[name='category']:checked").value ?? 0,
					// );

					// if (inputIdDom.value) {
					// 	await ctx.editAppointment(
					// 		inputIdDom.value,
					// 		inputTitleDom.value,
					// 		inputNotesDom.value,
					// 		inputAddressDom.value,
					// 		inputPhoneDom.value,
					// 		selectedCategory,
					// 		parseInt(inputDurationDom.value),
					// 		ctx.activeDay.val,
					// 	);
					// } else {
					// 	await ctx.addAppointment(
					// 		inputTitleDom.value,
					// 		inputNotesDom.value,
					// 		inputAddressDom.value,
					// 		inputPhoneDom.value,
					// 		selectedCategory,
					// 		parseInt(inputDurationDom.value),
					// 		ctx.activeDay.val,
					// 	);
					// }
					// ctx.onUpdate();

					ctx.simulateStartTimes(inputTimeDom.value);

					// @ts-ignore
					document.querySelector("#simulationModal").close();
				},
			},
			h3(t("simulation")),
			// inputIdDom,
			label(t("time")),
			inputTimeDom,
			// label(t("address")),
			// inputAddressDom,
			// label(t("phone")),
			// inputPhoneDom,
			// label(t("notes")),
			// inputNotesDom,
			// label(t("duration"), " (", t("minutes"), ")"),
			// inputDurationDom,
			// label(t("category")),
			// inputCategoryDom,
			button({ type: "submit" }, t("run_simulation")),
			// button({ 
			// 	onclick: () => {
			// 		(
			// 			document.querySelector("#simulationModal") as HTMLDialogElement
			// 		).close();
			// 	},
			// }, t("stop_simulation")),
		),
	);
};
