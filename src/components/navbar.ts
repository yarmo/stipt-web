import { Context } from "../context";
import { user } from "../icons";

export const navBar = (ctx: Context) => {
	const { div, nav, button } = ctx.van.tags;

	return nav(() => {
		const buttons = div(
			button(
				{
					class: `${ctx.activePage.val === "account" ? "active" : ""}`,
					onclick: () => {
						ctx.activePage.val =
							ctx.activePage.val === "app" ? "account" : "app";
					},
				},
				user,
			),
		);

		return !ctx.loggedInUser.val ? div() : buttons;
	});
};
