import van, { State } from "vanjs-core";
import { generateNavigationAppUrl } from "../helpers";
import * as icons from "../icons";
import { updateAppointmentDurationOffset } from "../server";
import { showEditModal } from "./appointmentModal";

export class AppointmentState {
	constructor(
		public id: string,
		public order: State<number>,
		public totalN: State<number>,
		public title: State<string>,
		public notes: State<string>,
		public address: State<string>,
		public phone: State<string>,
		public category: State<number>,
		public duration: State<number>,
		public durationOffset: State<number>,
		public move: State<number>,
		public deleted: State<boolean>,
		public startTime: State<string>,
		public progress: State<number>,
		public isOpen: State<boolean>,
	) {}
}

export const Appointment = (state: AppointmentState) => {
	const {
		id,
		order,
		totalN,
		title,
		notes,
		address,
		phone,
		category,
		duration,
		durationOffset,
		move,
		deleted,
		startTime,
		progress,
		isOpen,
	} = state;
	const { p, a, button, div, span } = van.tags;

	const durationOffsetString =
		durationOffset.val !== 0
			? ` [${duration.val}${durationOffset.val >= 0 ? "+" : ""}${
					durationOffset.val
			  }]`
			: "";

	const startTimeEl = startTime.val
		? span({ class: "timestamp" }, startTime.val)
		: null;

	return () =>
		deleted.val
			? null
			: div(
					{
						class: `appointment ${progress.val >= 0 ? "inProgress" : ""} ${
							progress.val === 0 ? "future" : ""
						} ${progress.val > 0 && progress.val < 100 ? "current" : ""} ${
							progress.val === 100 ? "past" : ""
						} ${duration.val + durationOffset.val === 0 ? "skip" : ""}`,
						"data-id": id,
						"data-is-open": isOpen,
						"data-category": category,
					},
					div(
						{ class: "appointmentProgress" },
						div({ style: `height: ${progress.val}%` }),
					),
					div(
						{ class: "appointmentWrapper" },
						p(
							{ class: "appointmentTitle" },
							startTimeEl,
							span({ class: "bold ellipsis" }, title),
							span({ class: "flex" }),
							span(
								{ class: "subtle" },
								`${
									duration.val + durationOffset.val
								}${durationOffsetString} min`,
							),
							div({ class: "appointmentCategory" }),
						),
						div(
							{ class: "appointmentContent" },
							div(
								{ class: "appointmentInformation" },
								() =>
									address.val
										? a(
												{
													href: generateNavigationAppUrl(address.val),
													target: "_blank",
													class: "appointmentAddress",
												},
												span({ class: "icon" }, icons.mapPin),
												span(address.val),
										  )
										: span(),
								() =>
									phone.val
										? a(
												{ href: `tel:${phone.val}`, class: "appointmentPhone" },
												span({ class: "icon" }, icons.phone),
												span(phone.val),
										  )
										: span(),
								() =>
									notes.val
										? p(
												{ class: "appointmentNotes" },
												span({ class: "icon" }, icons.info),
												span(notes.val),
										  )
										: span(),
							),
							div({ class: "appointmentButtons" }, () => {
								return isOpen.val
									? button(
											{
												onclick: () => {
													isOpen.val = false;
												},
											},
											icons.chevronUp,
									  )
									: button(
											{
												onclick: () => {
													isOpen.val = true;
												},
											},
											icons.chevronDown,
									  );
							}),
						),
						() => {
							return isOpen.val
								? div(
										div(
											{ class: "appointmentButtons" },
											button(
												{
													disabled: durationOffset.val === -duration.val,
													onclick: () => {
														durationOffset.val = Math.max(
															durationOffset.val - 5,
															-duration.val,
														);
														updateAppointmentDurationOffset(
															id,
															durationOffset.val,
														);
													},
												},
												"-5",
											),
											button(
												{
													disabled: durationOffset.val === -duration.val,
													onclick: () => {
														durationOffset.val = Math.max(
															durationOffset.val - 1,
															-duration.val,
														);
														updateAppointmentDurationOffset(
															id,
															durationOffset.val,
														);
													},
												},
												"-1",
											),
											button(
												{
													onclick: () => {
														durationOffset.val = 0;
														updateAppointmentDurationOffset(
															id,
															durationOffset.val,
														);
													},
												},
												"0",
											),
											button(
												{
													onclick: () => {
														durationOffset.val += 1;
														updateAppointmentDurationOffset(
															id,
															durationOffset.val,
														);
													},
												},
												"+1",
											),
											button(
												{
													onclick: () => {
														durationOffset.val += 5;
														updateAppointmentDurationOffset(
															id,
															durationOffset.val,
														);
													},
												},
												"+5",
											),
											button(
												{
													onclick: () => {
														durationOffset.val = -duration.val;
														updateAppointmentDurationOffset(
															id,
															durationOffset.val,
														);
													},
												},
												icons.slash,
											),
										),
										div(
											{ class: "appointmentButtons" },
											button(
												{
													disabled: order.val === 0,
													onclick: () => {
														move.val = -1;
													},
												},
												icons.arrowUp,
											),
											button(
												{
													disabled: order.val === totalN.val - 1,
													onclick: () => {
														move.val = 1;
													},
												},
												icons.arrowDown,
											),
											button(
												{
													onclick: () => {
														showEditModal(state);
													},
												},
												icons.edit,
											),
											button(
												{
													onclick: () => {
														deleted.val = true;
													},
												},
												icons.trash,
											),
										),
								  )
								: div();
						},
					),
			  );
};
