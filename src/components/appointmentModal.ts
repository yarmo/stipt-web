import { Context } from "../context";
import { t } from "../i18n";
import * as icons from "../icons";
import { AppointmentState } from "./appointment";

export const showAddModal = () => {
	const el: HTMLDialogElement | null =
		document.querySelector("#appointmentModal");
	if (!el) return;

	// @ts-ignore
	el.querySelector("form").reset();
	// @ts-ignore
	el.querySelector(".id").value = "";
	// @ts-ignore
	el.querySelector(".duration").value = 30;
	// @ts-ignore
	el.querySelector("#category-0").checked = true;
	// @ts-ignore
	el.showModal();
};

export const showEditModal = (appointment: AppointmentState) => {
	const el: HTMLDialogElement | null =
		document.querySelector("#appointmentModal");
	if (!el) return;

	// @ts-ignore
	el.querySelector("form").reset();
	// @ts-ignore
	el.querySelector(".id").value = appointment.id;
	// @ts-ignore
	el.querySelector(".title").value = appointment.title.val;
	// @ts-ignore
	el.querySelector(".notes").value = appointment.notes.val;
	// @ts-ignore
	el.querySelector(".address").value = appointment.address.val;
	// @ts-ignore
	el.querySelector(".phone").value = appointment.phone.val;
	// @ts-ignore
	el.querySelector(
		`[name="category"][value="${appointment.category.val}"]`,
		// @ts-ignore
	).checked = true;
	// @ts-ignore
	el.querySelector(".duration").value = appointment.duration.val;
	el.showModal();
};

export const appointmentModal = (ctx: Context) => {
	const { div, h3, span, button, dialog, form, input, textarea, label } =
		ctx.van.tags;

	const buttonCloseDom = button(
		{
			class: "close",
			onclick: () => {
				(
					document.querySelector("#appointmentModal") as HTMLDialogElement
				).close();
			},
		},
		icons.x,
	);
	const inputIdDom = input({ class: "id", type: "hidden" });
	const inputTitleDom = input({ class: "title", type: "text", autocapitalize: "words" });
	const inputNotesDom = textarea({ class: "notes" });
	const inputAddressDom = input({ class: "address", type: "text", autocapitalize: "words" });
	const inputPhoneDom = input({ class: "phone", type: "phone", autocapitalize: "words" });
	const inputDurationDom = input({
		class: "duration",
		type: "number",
		value: "30",
	});
	const inputCategoryDom = div(
		{ class: "categoryRadioWrapper" },
		input({
			type: "radio",
			id: "category-0",
			name: "category",
			value: "0",
			checked: true,
		}),
		label({ for: "category-0" }, span(icons.check)),
		input({ type: "radio", id: "category-1", name: "category", value: "1" }),
		label({ for: "category-1" }, span(icons.check)),
		input({ type: "radio", id: "category-2", name: "category", value: "2" }),
		label({ for: "category-2" }, span(icons.check)),
		input({ type: "radio", id: "category-3", name: "category", value: "3" }),
		label({ for: "category-3" }, span(icons.check)),
		input({ type: "radio", id: "category-4", name: "category", value: "4" }),
		label({ for: "category-4" }, span(icons.check)),
	);

	return dialog(
		{ id: "appointmentModal" },
		buttonCloseDom,
		form(
			{
				method: "dialog",
				onsubmit: async (evt: Event) => {
					evt.preventDefault();

					const selectedCategory = parseInt(
						// @ts-ignore
						document.querySelector("input[name='category']:checked").value ?? 0,
					);

					if (inputIdDom.value) {
						await ctx.editAppointment(
							inputIdDom.value,
							inputTitleDom.value,
							inputNotesDom.value,
							inputAddressDom.value,
							inputPhoneDom.value,
							selectedCategory,
							parseInt(inputDurationDom.value),
							ctx.activeDay.val,
						);
					} else {
						await ctx.addAppointment(
							inputTitleDom.value,
							inputNotesDom.value,
							inputAddressDom.value,
							inputPhoneDom.value,
							selectedCategory,
							parseInt(inputDurationDom.value),
							ctx.activeDay.val,
						);
					}
					ctx.onUpdate();

					// @ts-ignore
					document.querySelector("#appointmentModal").close();
				},
			},
			h3(t("appointment")),
			inputIdDom,
			label(t("title")),
			inputTitleDom,
			label(t("address")),
			inputAddressDom,
			label(t("phone")),
			inputPhoneDom,
			label(t("notes")),
			inputNotesDom,
			label(t("duration"), " (", t("minutes"), ")"),
			inputDurationDom,
			label(t("category")),
			inputCategoryDom,
			button({ type: "submit" }, t("submit")),
		),
	);
};
