import { Context } from "./context";
import { accountPage } from "./pages/account";
import { appPage } from "./pages/app";
import { landingPage } from "./pages/landing";

export const changePage = (ctx: Context, activePage: string) => {
	switch (activePage) {
		case "app":
			document.querySelector("#layout")?.replaceChildren(appPage(ctx));
			break;

		case "account":
			document.querySelector("#layout")?.replaceChildren(accountPage(ctx));
			break;
		default:
			document.querySelector("#layout")?.replaceChildren(landingPage(ctx));
			break;
	}
};
