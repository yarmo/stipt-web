import i18next from "i18next";
import enJson from "./locales/en/translations.json" assert { type: "json" };
import nlJson from "./locales/nl/translations.json" assert { type: "json" };

i18next.init({
	lng: "en",
	fallbackLng: "en",
	debug: true,
	resources: {
		en: {
			translation: enJson,
		},
		nl: {
			translation: nlJson,
		},
	},
});

export const changeLanguage = (lng: string) => {
	i18next.changeLanguage(lng);
};

export const t = (key: string) => {
	return i18next.t(key);
};
