import { Appointment } from "../components/appointment";
import { appointmentModal, showAddModal } from "../components/appointmentModal";
import { showSimulationModal, simulationModal } from "../components/simulationModal";
import { Context } from "../context";
import { getCurrentWeekday } from "../helpers";
import { t } from "../i18n";
import * as icons from "../icons";

export const appPage = (ctx: Context) => {
	const { h2, p, button, div, span } = ctx.van.tags;

	const currentWeekday = getCurrentWeekday();

	const startButton = button(
		{
			class: "main",
			onclick: () => {
				ctx.start();
			},
		},
		icons.play,
  	);

	startButton.addEventListener('long-press', function(e) {
		e.preventDefault();
		showSimulationModal();
	});

	return div(
		div(
			{ class: "buttonCollection" },
			() => {
				return ctx.lastStartTime.val
					? button(
							{
								class: "active main playing",
								onclick: () => {
									ctx.stop();
								},
							},
							icons.stop,
					  )
					: startButton
			},
			() =>
				button(
					{
						class: `${ctx.activeDay.val === 0 ? "active" : ""} ${
							currentWeekday === 0 ? "mark" : ""
						}`,
						onclick: () => {
							ctx.activeDay.val = 0;
						},
					},
					t("weekday_0").substring(0, 2),
				),
			() =>
				button(
					{
						class: `${ctx.activeDay.val === 1 ? "active" : ""} ${
							currentWeekday === 1 ? "mark" : ""
						}`,
						onclick: () => {
							ctx.activeDay.val = 1;
						},
					},
					t("weekday_1").substring(0, 2),
				),
			() =>
				button(
					{
						class: `${ctx.activeDay.val === 2 ? "active" : ""} ${
							currentWeekday === 2 ? "mark" : ""
						}`,
						onclick: () => {
							ctx.activeDay.val = 2;
						},
					},
					t("weekday_2").substring(0, 2),
				),
			() =>
				button(
					{
						class: `${ctx.activeDay.val === 3 ? "active" : ""} ${
							currentWeekday === 3 ? "mark" : ""
						}`,
						onclick: () => {
							ctx.activeDay.val = 3;
						},
					},
					t("weekday_3").substring(0, 2),
				),
			() =>
				button(
					{
						class: `${ctx.activeDay.val === 4 ? "active" : ""} ${
							currentWeekday === 4 ? "mark" : ""
						}`,
						onclick: () => {
							ctx.activeDay.val = 4;
						},
					},
					t("weekday_4").substring(0, 2),
				),
			() =>
				button(
					{
						class: `${ctx.activeDay.val === 5 ? "active" : ""} ${
							currentWeekday === 5 ? "mark" : ""
						}`,
						onclick: () => {
							ctx.activeDay.val = 5;
						},
					},
					t("weekday_5").substring(0, 2),
				),
			() =>
				button(
					{
						class: `${ctx.activeDay.val === 6 ? "active" : ""} ${
							currentWeekday === 6 ? "mark" : ""
						}`,
						onclick: () => {
							ctx.activeDay.val = 6;
						},
					},
					t("weekday_6").substring(0, 2),
				),
		),
		() => h2(t(`weekday_${ctx.activeDay.val}`)),
		() => {
			console.log("onStatefulBinding");
			ctx.onUpdate();

			const endTimeEl = ctx.computedEndTime.val
				? p(
						t("end"),
						": ",
						span(
							{ class: "timestamp" },
							ctx.computedEndTime.val.toLocaleTimeString("nl-NL", {
								hour: "2-digit",
								minute: "2-digit",
							}),
						),
				  )
				: null;

			return div(
				{ class: "timeline" },
				ctx.appointments[ctx.activeDay.val].val.map(Appointment),
				button(
					{
						onclick: () => {
							showAddModal();
						},
					},
					icons.plusCircle,
					t("add_new_appointment"),
				),
				endTimeEl,
			);
		},
		appointmentModal(ctx),
		simulationModal(ctx)
	);
};
