import { Context } from "../context";
import { t } from "../i18n";
import { logout } from "../server";

export const accountPage = (ctx: Context) => {
	const { h2, h3, p, button, div, select, option } = ctx.van.tags;

	return div(
		h2(t("user"), ": ", ctx.loggedInUser.val?.name),
		h3(t("settings")),
		p(t("change_language")),
		select(
			{
				onchange: (e) => {
					ctx.language.val = e.target.value;
					ctx.savePrefs();
				},
			},
			option(
				{ value: "en-US", selected: ctx.language.val === "en-US" },
				"English",
			),
			option(
				{ value: "nl-NL", selected: ctx.language.val === "nl-NL" },
				"Nederlands",
			),
		),
		h3(t("account")),
		button({ onclick: () => logout(ctx) }, t("log_out")),
	);
};
