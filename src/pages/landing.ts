import { Context } from "../context";
import { login } from "../server";

export const landingPage = (ctx: Context) => {
	const { h2, p, a, br, button, div, form, label, input } = ctx.van.tags;

	const inputEmailDom = input({ type: "text", placeholder: "email" });
	const inputPasswordDom = input({ type: "password", placeholder: "password" });

	return div(
		p("The Sequential Time Planning app"),
		h2("Log in"),
		form(
			{
				onsubmit: (evt: Event) => {
					evt.preventDefault();
					login(ctx, inputEmailDom.value, inputPasswordDom.value);
				},
			},
			label("Email address"),
			inputEmailDom,
			label("Password"),
			inputPasswordDom,
			button({ type: "submit" }, "Log in"),
		),
		h2("About Stipt"),
		p(
			"Stipt is an experimental app to plan appointments in a linear, sequential manner.",
		),
		p(
			"Source code: ",
			a(
				{ href: "https://codeberg.org/yarmo/stipt-web" },
				"codeberg.org/yarmo/stipt-web",
			),
			br,
			"License: ",
			a(
				{ href: "https://codeberg.org/yarmo/stipt-web/src/branch/dev/LICENSE" },
				"GPLv3",
			),
			br,
			"Framework: ",
			a({ href: "https://vanjs.org" }, "VanJS"),
			br,
			"Theme: ",
			a({ href: "https://rosepinetheme.com" }, "Rose pine"),
			br,
			"Icons: ",
			a({ href: "https://feathericons.com" }, "Feather"),
			br,
			"Backend: ",
			a({ href: "https://appwrite.io" }, "Appwrite"),
		),
	);
};
