import { Account, Client, Databases, ID, Query } from "appwrite";
import { AppointmentState } from "./components/appointment";
import { Context } from "./context";

export const client = new Client();

client
	// @ts-ignore
	.setEndpoint(import.meta.env.APPWRITE_ENDPOINT)
	// @ts-ignore
	.setProject(import.meta.env.APPWRITE_PROJECT);

export const account = new Account(client);
export const databases = new Databases(client);

export { ID } from "appwrite";

export const getCurrentAccount = async () => {
	return await account
		.get()
		.then((o) => o)
		.catch((_) => null);
};

export const login = async (ctx: Context, email: string, password: string) => {
	await account.createEmailSession(email, password);
	await account
		.get()
		.then((user) => {
			ctx.loggedInUser.val = user;
			ctx.activePage.val = "app";
			// changePage(ctx, 'app')
		})
		.catch((err) => {
			console.warn("Login failed: ", err);
		});
};

export const logout = async (ctx: Context) => {
	await account.deleteSession("current");
	ctx.loggedInUser.val = null;
	ctx.activePage.val = "landing";
};

export const setPrefs = async (ctx: Context) => {
	await account
		.updatePrefs({
			language: ctx.language.val,
			lastStartTime: ctx.lastStartTime.val,
		})
		.catch((_) => null);
};

export const getPrefs = async () => {
	return await account.getPrefs().catch((_) => null);
};

export const addAppointment = async (
	title: string,
	notes: string,
	address: string,
	phone: string,
	category: number,
	duration: number,
	weekday: number,
	order: number,
) => {
	return databases
		.createDocument("app", "appointments", ID.unique(), {
			title,
			notes,
			address,
			phone,
			category,
			duration,
			durationOffset: 0,
			weekday,
			order,
		})
		.catch((_) => null);
};

export const updateAppointment = async (
	id: string,
	appointment: AppointmentState,
) => {
	console.log(appointment);

	return databases
		.updateDocument("app", "appointments", id, {
			title: appointment.title.val,
			notes: appointment.notes.val,
			address: appointment.address.val,
			phone: appointment.phone.val,
			category: appointment.category.val,
			duration: appointment.duration.val,
			durationOffset: appointment.durationOffset.val,
		})
		.catch((_) => null);
};
export const updateAppointmentOrder = async (id: string, order: number) => {
	return databases
		.updateDocument("app", "appointments", id, { order })
		.catch((_) => null);
};
export const updateAppointmentTitle = async (id: string, title: string) => {
	return databases
		.updateDocument("app", "appointments", id, { title })
		.catch((_) => null);
};
export const updateAppointmentNotes = async (id: string, notes: string) => {
	return databases
		.updateDocument("app", "appointments", id, { notes })
		.catch((_) => null);
};
export const updateAppointmentAddress = async (id: string, address: string) => {
	return databases
		.updateDocument("app", "appointments", id, { address })
		.catch((_) => null);
};
export const updateAppointmentDuration = async (
	id: string,
	duration: number,
) => {
	return databases
		.updateDocument("app", "appointments", id, { duration })
		.catch((_) => null);
};
export const updateAppointmentDurationOffset = async (
	id: string,
	durationOffset: number,
) => {
	return databases
		.updateDocument("app", "appointments", id, { durationOffset })
		.catch((_) => null);
};

export const deleteAppointment = async (id: string) => {
	return databases.deleteDocument("app", "appointments", id).catch((_) => null);
};

export const getAppointments = async () => {
	return databases
		.listDocuments("app", "appointments", [
			Query.orderAsc("order"),
			Query.limit(2048),
		])
		.catch((_) => null);
};
