export const computeProgress = (
	startTime: Date,
	endTime: Date,
	currentTime: Date,
) => {
	const a = currentTime.getTime() - startTime.getTime();
	const b = endTime.getTime() - startTime.getTime();
	return Math.min(Math.max(Math.round((100 * a) / b), 0), 100);
};

export const getCurrentWeekday = () => (new Date().getDay() + 6) % 7;

export const generateNavigationAppUrl = (address: string) => {
	return `https://www.google.com/maps?${new URLSearchParams({
		q: address,
	}).toString()}`;
};
